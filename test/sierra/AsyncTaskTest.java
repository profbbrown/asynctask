package sierra;

import org.junit.Test;

import static org.junit.Assert.*;

public class AsyncTaskTest
{
    @Test
    public void testAsyncTask()
    {
        com.sun.javafx.application.PlatformImpl.startup(()->{});
        new Background().execute("hello");
        com.sun.javafx.application.PlatformImpl.exit();
    }

    private class Background extends AsyncTask<String, String>
    {
        public void onPreExecute()
        {
            long threadID = Thread.currentThread().getId();
            System.out.println("In onPreExecute thread ID " + threadID);
        }

        public String doInBackground(String s)
        {
            long threadID = Thread.currentThread().getId();
            System.out.println("In doInBackground thread ID " + threadID);
            return "from background";
        }

        public void onPostExecute(String s)
        {
            long threadID = Thread.currentThread().getId();
            System.out.println("In onPostExecute thread ID " + threadID);
        }
    }
}