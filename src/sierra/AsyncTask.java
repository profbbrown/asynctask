package sierra;

import javafx.application.Platform;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

abstract public class AsyncTask<Param, Result>
{
    private Param param;
    private Result result;
    private static ExecutorService executor;

    public AsyncTask()
    {
        if (executor == null)
        {
            ThreadFactory tf = new DaemonThreadFactory();
            executor = Executors.newSingleThreadExecutor(tf);
        }
    }

    protected void onPreExecute()
    {

    }

    protected Result doInBackground(Param param)
    {
        return null;
    }

    protected void onPostExecute(Result result)
    {

    }

    final public void execute(Param param)
    {
        this.param = param;

        PreExecuteTask pre = new PreExecuteTask();
        Platform.runLater(pre);
    }

    final public void cancel()
    {
        if (executor != null)
            executor.shutdownNow();
    }

    private class DaemonThreadFactory implements ThreadFactory
    {
        public Thread newThread(Runnable r)
        {
            Thread t = new Thread(r);
            t.setDaemon(true);
            return t;
        }
    }

    private class BackgroundTask implements Runnable
    {
        public void run()
        {
            result = doInBackground(param);
            PostExecuteTask post = new PostExecuteTask();
            Platform.runLater(post);
        }
    }

    private class PreExecuteTask implements Runnable
    {
        public void run()
        {
            onPreExecute();
            BackgroundTask t = new BackgroundTask();
            executor.execute(t);
        }
    }

    private class PostExecuteTask implements Runnable
    {
        public void run()
        {
            onPostExecute(result);
        }
    }
}